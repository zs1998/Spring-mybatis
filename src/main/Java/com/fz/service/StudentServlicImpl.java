package com.fz.service;

import com.fz.Mapper.StudentMapper;
import com.fz.entity.Student;
import com.fz.util.MybatisUtil;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import javax.net.ssl.SSLServerSocket;
import java.io.IOException;
import java.util.List;

/**
 * Created by xiao zeng on 2017/6/30.
 */
public class StudentServlicImpl implements StudentService {
    private SqlSession session;
    private StudentMapper studentdao;


    public StudentServlicImpl(){
        try {
            this.session = new MybatisUtil().getSession();
            this.studentdao = this.session.getMapper(StudentMapper.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }




    public List<Student> queryAll(int currage, int pagesize) {
        List<Student> list = studentdao.queryAll(currage,pagesize);
        this.session.commit();
        return list;
    }

    public int deleteByid(int id) {
        int n = studentdao.deleteByid(id);
        this.session.commit();
        return n;
    }

    public int add(Student student) {
        int f = studentdao.add(student);
        this.session.commit();
        return f;
    }

    public int count() {
        int c = studentdao.count();
        return c;
    }
}
