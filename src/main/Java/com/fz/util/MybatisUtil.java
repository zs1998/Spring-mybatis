package com.fz.util;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by xiao zeng on 2017/6/30.
 */
public class MybatisUtil {

     private SqlSessionFactory sf;
       private SqlSession session;

       public MybatisUtil() throws IOException {
           String resource = "mybatis-conf.xml";
           InputStream inputStream = Resources.getResourceAsStream(resource);
           this.sf = new SqlSessionFactoryBuilder().build(inputStream);
           this.session = this.sf.openSession();
       }

       public SqlSession getSession(){
           return this.session;
       }
}
