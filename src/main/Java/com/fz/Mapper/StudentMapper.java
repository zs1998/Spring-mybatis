package com.fz.Mapper;

import com.fz.entity.Student;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by xiao zeng on 2017/6/30.
 */
public interface StudentMapper {
  public List<Student> queryAll(@Param("currage") int currage,@Param("pagesize") int pagesize);

  public int deleteByid(int id);

  public int add(Student student);

  public int count();
}
