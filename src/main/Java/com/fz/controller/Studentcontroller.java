package com.fz.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fz.entity.Student;
import com.fz.service.StudentServlicImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by xiao zeng on 2017/6/30.
 */
@Controller
@RequestMapping("user")
public class Studentcontroller {

    //首页查询显示
    @RequestMapping("/indexs")
    public String indexs(HttpServletRequest req) {

        return "/WEB-INF/template/index";
    }

    @RequestMapping("/index")
    @ResponseBody
    //分页显示加json使用
    public String index(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        StudentServlicImpl sdao = new StudentServlicImpl();
        StringBuilder stu = new StringBuilder();
        int p = req.getParameter("p") == null ? 1 : Integer.parseInt(req.getParameter("p"));

        int shangye = p - 1;
        int xaye = p + 1;
        int pagesize = 2;
        int currage = p * pagesize - pagesize;
        int recordcount = sdao.count();
        int pagecount = recordcount % pagesize == 0 ? recordcount / pagesize : recordcount / pagesize + 1;

        if (shangye < 1) {
            shangye = 1;
        }
        if (xaye > pagecount) {
            xaye = pagecount;
        }
        List<Student> list = sdao.queryAll(currage, pagesize);
        for (Student s : list) {
            stu.append("<tr>");
            stu.append("<td>" + s.getId() + "</td>");
            stu.append("<td>" + s.getName() + "</td>");
            stu.append("<td>" + s.getScore() + "</td>");
            stu.append("<td><a href = \"/user/delete.do?id=" + s.getId() + "\">删除</td>");
            stu.append("</tr>");
        }
        stu.append("<tr>");
        stu.append("<td><a href =\"javascript:void(0);\" onclick=\"aaa($(this))\" v=\"" + shangye + "\">上一页</td>");
        stu.append("<td><a href =\"javascript:void(0);\" onclick=\"aaa($(this))\"  v=\"" + xaye + "\">下一页</td>");
        stu.append("<td>共" + pagecount + "页</td>");
        stu.append("</tr>");
        stu.append("<tr>");
        stu.append("<td><a href=\"/user/adds.action\">添加</a></td>");
        stu.append("</tr>");
        List<Map<String, Object>> l = new ArrayList<Map<String, Object>>();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("info", stu);
        l.add(map);

        return JSON.toJSONString(l);

    }

    @RequestMapping("/delete")
    public String dele(HttpServletRequest req) {
        if (req.getParameter("id") != null) {
            StudentServlicImpl sdao = new StudentServlicImpl();
            sdao.deleteByid(Integer.parseInt(req.getParameter("id")));
        }
        return "/index";
    }

    //接收表单值插入数据
    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String add(HttpServletRequest req, @RequestParam("user") String na) throws UnsupportedEncodingException {
        req.setCharacterEncoding("utf-8");

        String name = na;
        System.out.println(name);
        int score = Integer.parseInt(req.getParameter("score"));
        Student s = new Student();
        s.setName(name);
        s.setScore(score);
        StudentServlicImpl sdao = new StudentServlicImpl();
        sdao.add(s);

        return "/index";//带值转发到index控制器
    }

    //跳转到页面
    @RequestMapping("/adds")
    public String add() {
        return "/WEB-INF/template/add";
    }


}
